# SPA_BFF_Template

#### 介绍
.net对Spa应用支持的BFF层应用，提供Cors CSP XSS防护机制。轻松适配Vite构建的web应用

#### 分支说明 
Integrated-Identity-Services 集成OpenIddict搭建的授权服务，以及使用了yarp的反向代理方案
https://gitee.com/hardstylewyl/spa_bff_template/tree/Integrated-Identity-Services/

## 🎉 期待您的Pr以及Issue